import Vue from 'vue'
import Vuex from 'vuex'
import {alerts} from '@/store/modules/alerts';
import {userData} from '@/store/modules/userdata';
import {language} from '@/store/modules/language';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isUserLoggedIn: false,
    currentAlert: {},
    reRenderCounter: 0
  },
  mutations: {
    setIsUserLoggedIn: (state, bool) => {
      state.isUserLoggedIn = bool;
    },
    setCurrentAlert: (state, alert) => {
      state.currentAlert = alert;
    },
    reRender: (state) => {
      state.reRenderCounter++;
    }
  },
  modules: {
    alerts,
    userData,
    language
  }
});
