export const language = {
  namespaced: true,
  state: {
    currentLanguage: "pl",
    languages: ["en", "pl"],
  },
  mutations: {
    setLanguage: (state, language) => {
      if (state.languages.find((element) => element === language)) {
        state.currentLanguage = language;
      }
    },
  },
};
