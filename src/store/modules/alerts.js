import store from "../store";

export const alerts = {
  namespaced: true,
  state: {
    alerts: [
      { type: "success", text: "", alert: false },
      { type: "error", text: "", alert: false },
    ],
  },
  mutations: {
    manageSuccessAlert: (state, alert) => {
      state.alerts[0].alert = alert.bool;
      state.alerts[0].text = alert.message;
    },
    manageErrorAlert: (state, alert) => {
      state.alerts[1].alert = alert.bool;
      state.alerts[1].text = alert.message;
    },
    resetAlerts: (state) => {
      state.alerts.forEach((item) => {
        item.alert = false;
      });
    },
  },
  actions: {
    toggleSuccessAlert: ({ commit, state }, message) => {
      store.commit("setCurrentAlert", state.alerts[0]);
      commit("manageSuccessAlert", { bool: true, message: message });
      setTimeout(() => {
        commit("manageSuccessAlert", { bool: false });
      }, 3000);
    },
    toggleErrorAlert: ({ commit, state }, message) => {
      store.commit("setCurrentAlert", state.alerts[1]);
      commit("manageErrorAlert", { bool: true, message: message });
      setTimeout(() => {
        commit("manageErrorAlert", { bool: false });
      }, 3000);
    },
  },
};
