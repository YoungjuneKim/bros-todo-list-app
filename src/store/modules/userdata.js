import database from "@/database/userdata";
import store from "../store";

export const userData = {
  namespaced: true,
  state: {
    userData: {
      userId: "",
      userMail: "",
      userPassword: "",
      userCategories: [],
      language: "",
      todoPoints: 0,
      templateTodo: {
        category: "",
        duration: "",
        importance: "",
      },
      todos: [],
      completedTodos: 0,
      currentTodos: 0,
    },
  },
  mutations: {
    setLoggedUser: (state, userObject) => {
      state.userData = userObject;
    },
    setRegisteredUser: (state, userObject) => {
      state.userData.userId = userObject.userId;
      state.userData.userMail = userObject.userMail;
      state.userData.userPassword = userObject.userPassword;
    },
    setNewPassword: (state, newPassword) => {
      state.userData.userPassword = newPassword;
    },
    setUserLanguage: (state, language) => {
      state.userData.language = language;
    },
  },
  actions: {
    updateDatabase: ({ state }) => {
      database.users[state.userData.userId] = state.userData;
    },
    setLoggedUser: ({ commit }, userObject) => {
      commit("setLoggedUser", userObject);
      store.commit("language/setLanguage", userObject.language);
    },
  },
};
