import Vue from "vue";
import Router from "vue-router";
import Login from "../views/Login";
import Register from "../views/Register";
import Usersettings from "../views/Usersettings";
import Main from "../views/Main";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Main",
      component: Main,
      show: false,
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      show: true,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
      show: true,
    },
    {
      path: "/settings",
      name: "Settings",
      component: Usersettings,
      show: false,
    },
  ],
});
