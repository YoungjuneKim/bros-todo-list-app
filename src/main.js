import Vue from "vue";
import App from "./App";
import router from "./router/router";
import Vuetify from "vuetify";
import store from "@/store/store";
import "vuetify/dist/vuetify.min.css";

Vue.config.productionTip = false;
Vue.use(Vuetify, {
  iconfont: "md",
});

new Vue({
  el: "#app",
  router,
  store,
  vuetify: new Vuetify(),
  components: { App },
  template: "<App/>",
  render: (h) => h(App),
});
